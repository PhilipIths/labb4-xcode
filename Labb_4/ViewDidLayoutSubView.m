//
//  ViewDidLayoutSubView.m
//  Labb_4
//
//  Created by Stjernström on 2015-02-09.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ViewDidLayoutSubView.h"

@interface ViewDidLayoutSubView ()

@property (weak, nonatomic) IBOutlet UIView *Paddle;
@property (weak, nonatomic) IBOutlet UIView *Ball;
@property (nonatomic) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (nonatomic) float movement_x;
@property (nonatomic) float movement_y;
@property (nonatomic) int paddleSpeed;

@property (nonatomic) int points;
@property (weak, nonatomic) IBOutlet UILabel *ScoreCounter;
@property (weak, nonatomic) IBOutlet UILabel *GameOverText;

@end

@implementation ViewDidLayoutSubView

- (IBAction)onTap:(id)sender {
    
    CGPoint pos = [sender locationInView:self.view];
    [self updatePaddle:pos];
}

- (IBAction)onSwipe:(UIPanGestureRecognizer*)sender {
    
    CGPoint pos = [sender locationInView:self.view];
    [self updatePaddle:pos];
}

- (IBAction)onStartButtonClick:(id)sender {
    self.points = 0;
    self.ScoreCounter.text = [NSString stringWithFormat: @"Score: %d", self.points];
    self.GameOverText.hidden = YES;
    self.startButton.hidden = YES;
    [self setupBall];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateBall) userInfo:nil repeats:YES];
}

-(void)updatePaddle : (CGPoint) position {
    
    self.paddleSpeed = 12;
    
    // Move Left
    if (self.Paddle.center.x > position.x) {
        
        if (self.Paddle.center.x - position.x < self.paddleSpeed && self.Paddle.center.x - position.x != 0) {
            self.Paddle.center = CGPointMake(self.Paddle.center.x - 1, self.Paddle.center.y);
        }
        
        else {
            self.Paddle.center = CGPointMake(self.Paddle.center.x - self.paddleSpeed, self.Paddle.center.y);
        }
    }
    
    // Move Right
    if (self.Paddle.center.x < position.x) {
        
        if (position.x - self.Paddle.center.x < self.paddleSpeed && position.x - self.Paddle.center.x != 0) {
            self.Paddle.center = CGPointMake(self.Paddle.center.x + 1, self.Paddle.center.y);
        }
        
        else {
            self.Paddle.center = CGPointMake(self.Paddle.center.x + self.paddleSpeed, self.Paddle.center.y);
        }
    }
    
    // Wall Left
    if (self.Paddle.center.x < 53) {
        self.Paddle.center = CGPointMake(53, self.Paddle.center.y);
    }
    
    // Wall Right
    else if (self.Paddle.center.x > self.view.frame.size.width - 53) {
        self.Paddle.center = CGPointMake(self.view.frame.size.width - 53, self.Paddle.center.y);
    }
}

- (void)setupBall {
    
    self.movement_x = arc4random() %5;
    self.movement_x = _movement_x - 5;
    self.movement_y = arc4random() %5;
    self.movement_y = _movement_y - 5;
    
    if (self.movement_x == 0) {
        self.movement_x = 1;
    }
    
    if (self.movement_y == 0) {
        self.movement_y = 1;
    }
}

- (void)updateBall {
    
    self.Ball.center = CGPointMake(self.Ball.center.x + self.movement_x, self.Ball.center.y + self.movement_y);
    
    // Bounce Left
    if (self.Ball.center.x < 8) {
        self.movement_x = 0 - self.movement_x;
    }
    
    // Bounce Right
    if (_Ball.center.x > self.view.frame.size.width - 8) {
        self.movement_x = 0 - self.movement_x;
    }
    
    // Bounce Top
    if (self.Ball.center.y < 8) {
        self.movement_y = 0 - self.movement_y;
        self.points++;
        self.ScoreCounter.text = [NSString stringWithFormat: @"Score: %d", self.points];
    }
    
    // Hit Bottom - Game Over
    if (self.Ball.center.y > self.view.frame.size.height - 8) {
        
        self.movement_x = 0;
        self.movement_y = 0;
        self.Ball.center = CGPointMake(self.view.center.x, self.view.center.y);
        [self.timer invalidate];
        self.startButton.hidden = NO;
        self.GameOverText.hidden = NO;
        //[self performSegueWithIdentifier:@"EndGame" sender:self];
    }
    
    // Bounce Paddle
    if (CGRectIntersectsRect(self.Ball.frame, self.Paddle.frame)) {
        
        if (self.Ball.center.x > self.Paddle.center.x + 15) {
            self.movement_x = self.movement_x + 2;
        }
        
        else if (self.Ball.center.x < self.Paddle.center.x - 15) {
            self.movement_x = self.movement_x - 2;
        }
        
        else {
            self.movement_y = self.movement_y + 1;
        }
        
        //self.movement_y = arc4random() %5;
        //self.movement_x = arc4random() %5;
        self.movement_y = 0 - self.movement_y;
        //self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateBall) userInfo:nil repeats:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.points = 0;
    self.ScoreCounter.text = [NSString stringWithFormat: @"Score: %d", self.points];
    self.GameOverText.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}*/

@end
