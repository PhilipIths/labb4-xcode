//
//  GameOver.m
//  Labb_4
//
//  Created by Stjernström on 2015-02-16.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "GameOver.h"

@interface GameOver ()

@property (weak, nonatomic) IBOutlet UILabel *ScoreText;

@end

@implementation GameOver

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ScoreText.text = [NSString stringWithFormat: @"Score: %d", self.finalScore];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
