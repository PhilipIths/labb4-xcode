//
//  GameOver.h
//  Labb_4
//
//  Created by Stjernström on 2015-02-16.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameOver : UIViewController

@property (nonatomic) int finalScore;

@end
