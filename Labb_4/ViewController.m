//
//  ViewController.m
//  Labb_4
//
//  Created by Stjernström on 2015-02-09.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
