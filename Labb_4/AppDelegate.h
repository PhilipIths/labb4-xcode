//
//  AppDelegate.h
//  Labb_4
//
//  Created by Stjernström on 2015-02-09.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

